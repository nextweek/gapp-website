import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import Heading from '@theme/Heading';
import styles from './index.module.css';

function AppGalleryItem({ appId, helpLink, storeLink }) {
  return (
    <div className={clsx('app', styles.app)} id={appId}>
      <a href={helpLink} className={clsx('help-link', styles.helpLink)}>
        使用帮助
      </a>
      <a href={storeLink} className={clsx('store-link', styles.storeLink)}>
        商店详情
      </a>
    </div>
  );
}

export default function Home() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container text--center"> {/* 添加 text--center 类 */}
          <img
            src="img/banner.png"
            alt="Banner 图"
            className={clsx('banner-image')}
          />
          <div className="text--center"> {/* 添加 text--center 类 */}
            <Heading as="h1" className={clsx('hero-title')}>
              {siteConfig.tagline}
            </Heading>
          </div>
        </div>
      </header>
      <main className='text--center'>
        <div className={clsx('app-gallery', styles.appGallery)}>
          <AppGalleryItem
            appId="app1"
            helpLink="#"
            storeLink="#"
          />
          {/* 添加更多 AppGalleryItem */}
        </div>
      </main>
    </Layout>
  );
}