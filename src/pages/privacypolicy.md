---
id: privacypolicy
slug: /privacypolicy
title: Privacy Policy
---

# Privacy Policy
Last Updated：December 30, 2023

Thank you for selecting Home Workout - No Equipment (referred to as “Service” or “Application”). We take the privacy and protection of your information very seriously. This policy outlines how we collect, utilize, and safeguard your information. Prior to using our App, we kindly ask you to thoroughly review this policy. By using our app, you agree and accept our privacy policy.

This Privacy Policy is applicable to the Home Workout - No Equipment provided by GAPP Team (referred to as "We" or "Developer"). Throughout this policy, we will clarify the following:

1. Definitions
You refer to the individual accessing or using the Service, or the Developer or other legal entity accessing or using the Service on behalf of that individual, if applicable.

We (referred to as either "we," "us," "developer," or "our" in this policy) refer to GAPP Team Team.

Application denotes an App named Home Workout - No Equipment, provided by us, which you can download onto electronic devices.

Personal data refers to any information that can identify a specific individual.

Service refers to our Application.

Third-party Media Service signifies any website or social networking platform where users can log in or create an account to access the Service.

2. Information and Permissions
The information we collect depends on how you use our services. We provide some optional permissions to enhance your experience and you are free to enable these permissions to access the best features. Please note that even without these permissions, you can still enjoy our services.

App Permissions (Optional)
Notification Permission (For Android 13 & Above Android Version): If you want to receive alarms or other reminders from us, you need to enable notification permission.
The Information You Provide
When using the app, you can choose to enter your gender, age, height, weight, etc. This information is used to calculate your BMI and calories burned during exercise. You can update this information at any time in the app settings to ensure your workout reports are more accurate. Please note that these are optional for you. If you choose not to fill in the relevant information, it will not affect the main functions of the app, but it may slightly reduce the quality of your experience.

All interactive actions within our app are referred to as "Your Activities," including your settings within the app (such as preferred language) and the time you spend in the app interface (which helps us improve the features you frequently visit). We use your activities and the information you provide to offer you more personalized services while continuously improving our app.

Third-Party Login Information
Our app can be used without registration or login. You can also choose to back up your exercise data by logging in with a Google account. In this case, we may automatically collect your account name and other publicly available information related to your account. Please note that the backup feature does not automatically sync in real time, it is activated when the user chooses to log into the app or manually backup the data.

Feedback Information
You can improve your experience with the app by sending us feedback. Feedback information may include your usage information, such as app version, system version, screen size, country/region, app language, device, time zone, network status, accounts, and whether you have activated the ad removal service. The decision to send feedback is entirely voluntary. We do Not collect this information unless you proactively provide feedback and we regularly delete this feedback information, once a month.

3. The Way We Share the Information
We NEVER share the information you provide in our app with any third parties. We may display personalized ads in our app to support and enhance our services. Currently, our advertising partner is Google, and for advertising, analysis, and fraud prevention purposes, Google may collect certain personal data, such as online identifiers and IP addresses. Before collecting this information, Google will seek your consent. Google is obligated not to disclose this information and not to use it for other purposes.

You can choose not to receive personalized ads. However, after doing so, you will still see ads, but the relevance of the ads to your interests and preferences will be reduced.

To learn more about “How Google uses information from sites or apps that use our services”, please visit this page: https://policies.google.com/technologies/partner-sites?hl=en

To enable or disable personalized ads by Google, please visit this page: https://myadcenter.google.com/personalizationoff?sasb=true&ref=ad-settings

For a detailed understanding of Google's privacy policy, please visit this page: https://policies.google.com/privacy?hl=en

4. How Do We Use the Information
We use the information you provide to offer you more personalized services. We care about your experience and continually improve our features based on your needs. The information you provide helps us understand what needs improvement and how to enhance our offerings.

We might use this information for:

Improving our services to make your experience more personalized.
Ensuring the smooth running of our app and fixing any issues that may arise.
Providing support based on user feedback.
Fulfilling any contracts you've entered into with us, including purchase agreements for products or services through our platform.
Communicating with you, sharing information related to app improvements, contract updates, and necessary security enhancements.
In cases where the law requires it, we may cooperate with law enforcement agencies and public authorities by sharing information. We rigorously review the legality and validity of such requests and only provide information to meet specific investigative purposes and solely for law enforcement access.
5. Payment Method
We may offer paid products and/or services within our Service. In such cases, we may utilize third-party services for payment processing, such as payment processors.

Ensuring the security of your payments is of utmost importance to us. Therefore, we do not store or collect your payment card details. Instead, your payment card details are directly provided to our third-party payment processors. These payment processors handle your information in accordance with their own privacy policies and are bound by the PCI Data Security Standards established by the PCI Security Standards Council, a collaborative effort by brands such as Visa, Mastercard, American Express, and Discover. The PCI Data Security Standards require measures to help ensure the secure handling of payment information.

By adhering to the PCI Data Security Standards, the payment processors have implemented a range of security measures to safeguard your payment information. These measures contribute to the secure processing of payment information, protecting your privacy and security.

Google Play In-App Payments
Their Privacy Policy can be viewed at https://www.google.com/policies/privacy/

6. Transfer of Your Information
Your information may be processed in the developer's operating offices and other locations where the involved parties are situated. This means that your information may be transferred to and maintained on computers located outside of your state, province, country, or other governmental jurisdiction, where information protection laws may differ from those of your jurisdiction. The developer will take all reasonable steps to ensure the secure treatment of your information in accordance with this Privacy Policy.

We may need to share or transfer your information in case of mergers, sale of company assets, financing, or acquisition of all or a portion of our business. We will notify you in advance and give you at least two weeks to decide whether you want to retain or completely delete your account and information. Please note that we will only share or transfer your information with your consent.

7. Retention of Your Information
Different types of information are stored and deleted in various ways.

The Information You Provide (include Information from Health Connect)
If you haven't used the backup feature, the information is stored only on your device and is permanently deleted after you uninstalling the app.

If you have used the login and backup features, you can permanently delete your information from your account by clicking "Delete all data" through the setting page.

Third-Party Login Information
The "Delete Account" function enables you to permanently delete third-party login information.

Feedback Information
In general, we do not retain the feedback information you provide. In cases where information retention is necessary for app function improvements, we will delete it once a month.

8. Security
We have implemented technical and organizational measures to safeguard against unauthorized or unlawful processing of your information, as well as accidental or unlawful destruction, loss, alteration, unauthorized disclosure, or access to your information. It is important to note that while we take precautions to protect your information, we cannot guarantee its absolute security.

9. Links to Other Websites
Our Service may include links or provide access to third-party websites (or third-party sites) for your convenience. We have no control over, do not review, and are not responsible for the content, goods, or services offered by third-party websites. Your use of third-party websites and any information you provide to them are done at your own risk. We recommend that you review the privacy policies of any third-party websites with which you interact.

10. GDPR Privacy
  •   Right to Be Informed
When collecting your personal data, you have the right to know our identity, the purposes and methods of processing your personal data, and how to exercise and protect your rights.

  •   Right of Access
You have the right to request confirmation of whether we are processing your personal data and to access personal data and related information (e.g., categories of personal data processed).

  •   Right to Rectification
If your personal data is inaccurate, you have the right to request rectification and to complete any incomplete personal data, as permitted by law.

  •   Right to Erasure
You have the right to request the deletion of your personal data, where permitted by law. This right can be exercised, among other circumstances, when your personal data is no longer necessary for the purposes for which it was collected or processed unlawfully.

  •   Right to Restriction of Processing
You have the right to request the restriction of our processing in certain limited circumstances, including when the accuracy of your personal data is contested or when the processing is unlawful, and you oppose the erasure of your personal data.

  •   Right to Information Portability
You have the right to receive the personal data you have provided to us in a structured, commonly used, and machine-readable format, and you have the right to transmit this information to another information controller, where technically feasible and legally permitted.

  •   Right to Object
You have the right to object to our processing of your personal data, where permitted by law. This right is limited to processing based on legitimate interests. Subsequently, we will no longer process your personal data unless we can demonstrate compelling legitimate grounds for the processing that override your interests, rights, and freedoms, or for the establishment, exercise, or defense of legal claims. Additionally, you have the right to object at any time to the processing of personal data for direct marketing purposes.

11. Your California Privacy Rights
  •   Right to Access Personal data
You have the right to request confirmation of whether we are processing your personal data and where permitted by law, access to that personal data. This may include the personal data categories we collect, use, or disclose about you.

  •   Right to Deletion
Within the limits allowed by law, you have the right to request the deletion of your personal data.

  •   Right to Opt-Out of "Selling" and Certain Sharing Practices
You have the right to opt out of sharing certain information with third parties who may only use your personal data for their purposes. Your right to opt-out applies only to information we "sell" to these third parties. In this context, "selling" does not mean exchanging information for money—we do not engage in such practices. "Selling" refers to the disclosure of information when a third party may use it for their own purposes, such as personalized advertising, including non-identifiable technical device information. We do not actually sell the personal data of individuals under 16 years old.

  •   Right to Non-Discrimination
We strongly oppose discrimination of any kind, including but not limited to discrimination based on race, color, ethnicity, nationality, religion, gender, sexual orientation, age, marital status, disability, or any other protected characteristic under the law. If you encounter any instances of discrimination, you have the right to request appropriate corrective measures from us, including but not limited to investigation, correction, deletion of relevant information, or other reasonable corrective actions. We are committed to ensuring a fair, equal, and inclusive service environment to promote positive experiences and engagement for all users.

12. Children's Privacy
We do not knowingly collect personal data from individuals under the age of 13 online. If you become aware that a child has provided us with personal data without parental consent, please contact our support team. In the event that we discover an individual under the age of 13 has provided us with personal data without parental consent, we will promptly delete the information and deactivate the individual's account.

13. Privacy Policy Changes
We may periodically update our Privacy Policy. Before the changes take effect, we will inform you through email and/or prominent notice on our Service, and update the "Last Updated" date at the top of this Privacy Policy.

We recommend checking this Privacy Policy regularly to stay informed about any changes. If you continue to use our app after the policy update, it signifies your acceptance of our privacy policy.

14. Contact Us
If you have any questions or concerns about our Privacy Policy, please don't hesitate to reach out to us using the following contact information: hlinlin5200@gmail.com. We will promptly respond to your inquiries and address your concerns.